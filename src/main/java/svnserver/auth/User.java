/**
 * This file is part of git-as-svn. It is subject to the license terms
 * in the LICENSE file found in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/gpl-2.0.html. No part of git-as-svn,
 * including this file, may be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file.
 */
package svnserver.auth;

import org.jetbrains.annotations.NotNull;

/**
 * User. Just user.
 *
 * @author Marat Radchenko <marat@slonopotamus.org>
 */
public class User {
    @NotNull
    private String uniqueName;
    @NotNull
    private String name;
    @NotNull
    private String email;

    private long uid;

    public void NameView(@NotNull String realName) {
        this.name = realName;
    }

    public long UID() {
        return uid;
    }

    public User(@NotNull String name, @NotNull String email, long uid) {
        this.name = name;
        this.uid = uid;
        this.uniqueName = String.format("uid-%d", uid);
        this.email = email;
    }

    @NotNull
    public String UniqueName() {
        return uniqueName;
    }

    @NotNull
    public String Name() {
        return name;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public boolean isAnonymous() {
        return email.equals("unknown");
    }

    @Override
    public String toString() {
        return uniqueName;
    }
}
