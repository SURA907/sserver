/*
 * File: VcsBranchMode.java
 * Author: Force<forcemz@outlook.com>
 * CreateTime: @2015.1
 * Copyright (c) 2015. OSChina.NET.All Rights Reserved.
 */

package svnserver.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface VcsBranchMode {
  @Nullable
  String getDefaultBranch();

  String TranslateSubversionBranchName(@NotNull String svnBranch);

  String BranchNameRestore(@NotNull String gitBranch);
}
