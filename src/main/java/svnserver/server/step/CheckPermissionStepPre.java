/*
 * Copyright (c) 2015. OSChina.NET.All Rights Reserved.
 * Author: Force<forcemz@outlook.com>
 */

package svnserver.server.step;

import org.jetbrains.annotations.NotNull;
import org.tmatesoft.svn.core.SVNException;
import svnserver.server.SessionContext;

import java.io.IOException;

/**
 * Owner Token Check Repository Status.
 * * *
 */
final public class CheckPermissionStepPre implements Step {
  @NotNull
  private final Step nextStep;

  public CheckPermissionStepPre(@NotNull Step nextStep) {
    this.nextStep = nextStep;
  }

  @Override
  public void process(@NotNull SessionContext context) throws IOException, SVNException {
    context.checkAcl(context.getRepositoryPath(""));

    context.getWriter().listBegin().word("success").listBegin().listBegin().listEnd().string("").listEnd().listEnd();
    nextStep.process(context);
  }
}
