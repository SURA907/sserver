/*
 * File: SubversionPath.java
 * Author: Force Charlie
 * CreateTime: @2015.10
 * Copyright (C) 2018. GITEE.COM.All Rights Reserved.
 */

package svnserver.service;

import org.jetbrains.annotations.NotNull;
import org.tmatesoft.svn.core.SVNURL;
import java.util.StringTokenizer;

public class SubversionPath {
  private ReferenceIndex refsIndex;
  private String owner;
  private String name;
  private String branch = "";
  private String prefix;
  private String burl;

  public String RepositoryPath() {
    String sp = Parameters.Get().getRoot() + this.RepoOwner().substring(0, 2) + "/" + this.RepoOwner() + "/"
        + this.RepoName() + ".git";
    return sp;
  }

  public ReferenceIndex RefIndex() {
    return refsIndex;
  }

  @NotNull
  public String RepoOwner() {
    return owner;
  }

  @NotNull
  public String RepoName() {
    return name;
  }

  @NotNull
  public String Branch() {
    return branch;
  }

  @NotNull
  public String Prefix() {
    return prefix;
  }

  @NotNull
  public String BaseURL() {
    return burl;
  }

  public static SubversionPath Builder(@NotNull SVNURL svnurl) {
    String path = svnurl.getPath();
    SubversionPath sp = new SubversionPath();
    StringTokenizer stringTokenizer = new StringTokenizer(path, "/");
    if (stringTokenizer.countTokens() < 2)
      return null;
    /**
     * Owner and Repository name get
     */
    sp.owner = stringTokenizer.nextToken();
    sp.name = stringTokenizer.nextToken();

    StringBuilder burl = new StringBuilder();

    burl.append(svnurl.getProtocol() + "://");
    if (svnurl.getUserInfo() != null) {
      burl.append(svnurl.getUserInfo());
      burl.append("@");
    }
    ////
    if (Parameters.Get().IsFilterDomain()) {
      burl.append(Parameters.Get().Domain());
    } else {
      burl.append(svnurl.getHost());
    }
    burl.append("/");
    burl.append(sp.RepoOwner());
    burl.append("/");
    burl.append(sp.RepoName());

    int counts = stringTokenizer.countTokens();
    if (counts == 0) {
      sp.refsIndex = ReferenceIndex.ReferenceHEAD;
      sp.prefix = "/";

      sp.burl = burl.toString();
      return sp;
    }

    String rflags = stringTokenizer.nextToken();
    //////////////////////////////////////
    if (rflags == null)
      return null;
    switch (rflags) {
    case "trunk": {
      sp.refsIndex = ReferenceIndex.ReferenceTrunk;
      sp.branch = "master";

      burl.append("/trunk");
      sp.burl = burl.toString();
      if (stringTokenizer.countTokens() == 0) {
        sp.prefix = "/";
        return sp;
      }
      StringBuilder prefix = new StringBuilder();
      while (stringTokenizer.countTokens() > 0) {
        prefix.append("/");
        prefix.append(stringTokenizer.nextToken());
      }
      sp.prefix = prefix.toString();
    }
      break;
    case "branches": {
      if (stringTokenizer.countTokens() == 0)
        return null;
      sp.refsIndex = ReferenceIndex.ReferenceBranch;
      String branches = stringTokenizer.nextToken();
      sp.branch = branches;

      burl.append("/branches/");
      burl.append(branches);
      sp.burl = burl.toString();

      if (stringTokenizer.countTokens() == 0) {
        sp.prefix = "/";
      } else {
        StringBuilder prefix = new StringBuilder();
        while (stringTokenizer.countTokens() > 0) {
          prefix.append("/");
          prefix.append(stringTokenizer.nextToken());
        }
        sp.prefix = prefix.toString();
      }

    }
      break;
    case "tags": {
      if (stringTokenizer.countTokens() == 0)
        return null;
      sp.refsIndex = ReferenceIndex.ReferenceTags;
      String tags = stringTokenizer.nextToken();
      sp.branch = tags;

      burl.append("/tags/");
      burl.append(tags);
      sp.burl = burl.toString();
      // svnPathInfo.setBaseURL(burl);
      if (stringTokenizer.countTokens() == 0) {
        sp.prefix = "/";
      } else {
        StringBuilder prefix = new StringBuilder();
        while (stringTokenizer.countTokens() > 0) {
          prefix.append("/");
          prefix.append(stringTokenizer.nextToken());
        }
        sp.prefix = prefix.toString();
      }
    }
      break;
    default: {
      sp.refsIndex = ReferenceIndex.ReferenceHEAD;
      sp.burl = burl.toString();
      StringBuilder prefix = new StringBuilder();
      prefix.append("/");
      prefix.append(rflags);
      while (stringTokenizer.countTokens() > 0) {
        prefix.append("/");
        prefix.append(stringTokenizer.nextToken());
      }
      sp.prefix = prefix.toString();
    }
      break;
    }
    return sp;
  }
}