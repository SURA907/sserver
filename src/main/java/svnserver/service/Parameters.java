package svnserver.service;

import org.jetbrains.annotations.NotNull;
import org.mapdb.DB;
import org.mapdb.DBException;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import svnserver.server.SvnServer;

import java.io.File;

public class Parameters {
  //
  static private Parameters inst_ = null;
  private static final Logger log = LoggerFactory.getLogger(SvnServer.class);

  public static boolean Initialize(@NotNull String file) {
    TomlUtils utils = new TomlUtils();
    if (!utils.ParseFile(file, false)) {
      return false;
    }
    inst_ = new Parameters();
    inst_.shuttime = utils.get("Service.ShutdownTimeout", 10l);
    inst_.rlimit = utils.lookupSizeBytes("Service.LimitedSize", 400);
    inst_.urlPrefix = utils.get("Service.ApiPrefix", "http://gitee.com/api/v3/");
    inst_.domain = utils.get("Service.Domain", "gitee.com");
    inst_.timeout = (int) utils.get("Service.Timeout", 1800l); // 1800s
    inst_.flimit = utils.lookupSizeBytes("Service.FileLimitSize", 100l);
    long MP = Runtime.getRuntime().availableProcessors();
    inst_.threads = (int) utils.get("App.Threads", MP);
    inst_.port = (int) utils.get("App.Port", 3690l);
    inst_.address = utils.get("App.Address", "0.0.0.0");
    inst_.root = utils.get("App.Root", "/home/git/repositories/");
    inst_.useCachedThreadPool = utils.get("App.UseCachedThreadPool", false);
    inst_.filterDomain = utils.get("App.FilterDomain", false);
    inst_.enableSync = utils.get("App.EnableSync", false);
    inst_.enableSizeEvent = utils.get("App.EnableSizeEvent", false);

    inst_.redisServer = utils.get("App.RedisServer", "redis://127.0.0.1:6379");
    /// Old config support...
    if (!inst_.redisServer.startsWith("redis://")) {
      inst_.redisServer = "redis://" + inst_.redisServer;
    }
    inst_.sshallowed = utils.get("SSH.Allowed", false);
    inst_.sshusetoken = utils.get("SSH.UseToken", true);
    inst_.sshtoken = utils.get("SSH.Token", "");
    try {
      File f = new File(file);
      inst_.basePath = f.getAbsoluteFile().getParentFile();
    } catch (Exception e) {
      log.error("Initialize", e);
      return false;
    }
    return true;
  }

  @NotNull
  public static Parameters Get() {
    return inst_;
  }

  public long Shuttime() {
    return shuttime;
  }

  public long Rlimit() {
    return rlimit;
  }

  public String URLPrefix() {
    return urlPrefix;
  }

  public int Threads() {
    return threads;
  }

  public String Domain() {
    return domain;
  }

  public String getRoot() {
    return root;
  }

  public String RedisHost() {
    return redisServer;
  }

  public int Port() {
    return port;
  }

  public String ListenAddress() {
    return address;
  }

  public int Timeout() {
    return timeout;
  }

  public long Flimit() {
    return flimit;
  }

  public boolean IsUseCachedThreadPool() {
    return useCachedThreadPool;
  }

  public boolean IsEnableSync() {
    return enableSync;
  }

  public boolean EnableSizeEvent() {
    return enableSizeEvent;
  }

  public boolean IsFilterDomain() {
    return filterDomain;
  }

  @NotNull
  public File BasePath() {
    return basePath;
  }

  public boolean SshIsAllowed() {
    return sshallowed;
  }

  public boolean SshNeedToken() {
    return sshusetoken;
  }

  @NotNull
  public String SshToken() {
    return sshtoken;
  }

  private String urlPrefix;
  private String domain;
  private String address;
  private String root;
  private String redisServer;
  private int port;
  private int threads;
  private int timeout;
  private long flimit;
  private long rlimit;
  private long shuttime;
  private File basePath;
  private boolean useCachedThreadPool;
  private boolean filterDomain;
  private boolean enableSync;
  private boolean enableSizeEvent;
  private boolean sshallowed;
  private boolean sshusetoken;
  private String sshtoken;

  @NotNull
  public static File joinPath(@NotNull File basePath, @NotNull String localPath) {
    final File path = new File(localPath);
    return path.isAbsolute() ? path : new File(basePath, localPath);
  }

  @NotNull
  static public DB createCache(@NotNull File basePath) {
    try {
      return DBMaker.fileDB(joinPath(basePath, "svn-cache.db")).closeOnJvmShutdown().fileMmapEnableIfSupported().make();
    } catch (DBException e) {
      throw new DBException(String.format("Failed to open %s: %s", basePath, e.getMessage()), e);
    }
  }
}
