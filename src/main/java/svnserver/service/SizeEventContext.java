package svnserver.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

public class SizeEventContext {
    @JsonProperty("path")
    public String Path;
    @JsonProperty("pwn")
    public String Pwn;
    @JsonProperty("size")
    public long Size;

    public SizeEventContext(@NotNull String path, @NotNull String pwn, long size) {
        this.Path = path;
        this.Pwn = pwn;
        this.Size = size;
    }
};