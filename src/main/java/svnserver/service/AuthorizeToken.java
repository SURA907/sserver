///
package svnserver.service;


import com.fasterxml.jackson.annotation.JsonProperty;

import org.jetbrains.annotations.NotNull;

public class AuthorizeToken {
    @JsonProperty("pwn")
    public String PWN;
    @JsonProperty("username")
    public String Username;
    @JsonProperty("password")
    public String Password;
    @JsonProperty("branch")
    public String Branch;
    /// FIXME when update remove repopath
    @JsonProperty("repo_path")
    public String RepoPath;
    public AuthorizeToken(String pwn, String user, String pwd, @NotNull String branch) {
        PWN=pwn;
        Username = user;
        Password = pwd;
        Branch = branch;
        //
        RepoPath = pwn;
    }
}