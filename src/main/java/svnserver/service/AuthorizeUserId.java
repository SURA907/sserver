///
package svnserver.service;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.jetbrains.annotations.NotNull;

public class AuthorizeUserId {
    @JsonProperty("pwn")
    public String PWN;
    @JsonProperty("branch")
    public String Branch;
    @JsonProperty("uid")
    public int UID;
    //NOTICE remove this code when update
    @JsonProperty("repo_path")
    public String RepoPath;
    @JsonProperty("user_id")
    public int Userid;
    public AuthorizeUserId(@NotNull String pwn, int uid, @NotNull String branch) {
        UID = uid;
        Branch = branch;
        PWN=pwn;
        // When update we will remove
        RepoPath = pwn;
        Userid=uid;
    }
}