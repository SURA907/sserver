/*
 * File: SessionContext.java
 * Author: Force Charlie
 * CreateTime: @2015.10
 * Copyright (C) 2018. GITEE.COM. All Rights Reserved.
 */

package svnserver.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import svnserver.auth.User;

public class AuthorizeResult {
  public static final int OK = 0;
  public static final int NotFound = 1;
  public static final int NotAllow = 2;
  public static final int AuthError = 3;
  public static final int BranchNotExists = 4;
  public static final int Denied = 5;
  public static final int Blocked = 6;
  public static final int RepoHuge = 7;
  public static final int InternalError = 8;
  public static final int Unknown = 999;
  @JsonProperty("result")
  private int result = -1;
  @JsonProperty("message")
  private String msg = "unkown error";

  @JsonProperty("uid")
  private long uid = -1;

  @JsonProperty("rid")
  private long rid = -1;

  @JsonProperty("writeable")
  private boolean writeable;

  @JsonProperty("name")
  private String name;

  @JsonProperty("email")
  private String email;

  @JsonProperty("rlimit")
  private long rlimit = 0;

  @JsonProperty("flimit")
  private long flimit = 0;

  @JsonProperty("path")
  private String repositoryPath;

  /// FIXME remove some code
  @JsonProperty("repo_limit")
  private long repoLimitSize = 0;

  @JsonProperty("file_limit")
  private long fileLimitSize = 0;

  @JsonProperty("user_id")
  private int userid = -1;

  public int ErrorCode() {
    return result;
  }

  public static AuthorizeResult ErrorNew(int err, @NotNull String msg) {
    AuthorizeResult result = new AuthorizeResult();
    result.result = err;
    result.msg = msg;
    return result;
  }

  public String RepositoryPath() {
    return repositoryPath;
  }

  public void RepositoryPath(String rpath) {
    // this.repositoryPath = repositoryPath;
    Path rpath_ = Paths.get(rpath);
    repositoryPath = rpath_.normalize().toString();
  }

  public long UID() {
    return uid;
  }

  public long RID() {
    return rid;
  }

  public String Message() {
    return msg;
  }

  public boolean Writeable() {
    return writeable;
  }

  public String NameView() {
    return name;
  }

  public String Email() {
    return email;
  }

  /**
   * @return the repoLimitSize
   */
  public long Rlimit() {
    return rlimit;
  }

  protected static long Rlimitlocal(@NotNull String repodir) {
    String filepath = repodir + "/svn.toml";
    TomlUtils utils = new TomlUtils();
    if (!utils.ParseFile(filepath, true)) {
      return Parameters.Get().Rlimit();
    }
    return utils.lookupSizeBytes("Base.RepoLimit", 400);
  }

  public void UpdateView(@NotNull String path) {
    if (uid <= 0) {
      uid = userid;
    }
    long rlimitlocal = Rlimitlocal(path);
    long rlimittemp = 0, flimittemp = 0;
    if (rlimit > 0) {
      rlimittemp = rlimit * 1024 * 1024;
    } else if (repoLimitSize > 0) {
      rlimittemp = repoLimitSize * 1024 * 1024;
    }
    // Support for releasing some repository size limits
    rlimit = Math.max(rlimittemp, rlimitlocal);

    if (flimit > 0) {
      flimittemp = flimit * 1024 * 1024;
    } else if (fileLimitSize > 0) {
      flimittemp = fileLimitSize * 1024 * 1024;
    } else {
      flimittemp = Parameters.Get().Flimit();
    }
    flimit = flimittemp;
  }

  public long Flimit() {
    return flimit;
  }

  public User MakeUser() {
    return new User(name, email, uid);
  }

}
