package svnserver.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

public class BackupContext {
    @JsonProperty("pwn")
    public String Pwn;
    @JsonProperty("uuid")
    public boolean EnableUUID;

    public BackupContext(@NotNull String pwn, boolean allowuuid) {
        this.Pwn = pwn;
        this.EnableUUID = allowuuid;
    }
};