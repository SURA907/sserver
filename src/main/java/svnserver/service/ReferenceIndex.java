/*
 * File: ReferenceIndex.java
 * Author: Force Charlie
 * CreateTime: @2015.10
 * Copyright (C) 2018. GITEE.COM. All Rights Reserved.
 */

package svnserver.service;

public enum ReferenceIndex {
  ReferenceBranch, ReferenceTrunk, ReferenceTags, ReferenceHEAD
}
