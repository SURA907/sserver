package svnserver.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

public class RepositoryUtils {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(AuthorizeManager.class);

    @Nullable
    public static String DiscoverHead(@NotNull String rpath) {
        File f = Paths.get(rpath, "HEAD").toFile();
        if (!f.exists()) {
            return null;
        }
        FileReader fr;
        try {
            fr = new FileReader(f);
        } catch (FileNotFoundException e) {
            log.error("DiscoverHead", e);
            return null;
        }

        BufferedReader br = new BufferedReader(fr);
        try {
            String line = br.readLine();
            br.close();
            fr.close();
            if (line.startsWith("ref: refs/heads/")) {
                return line.substring("ref: refs/heads/".length());
            }
            return null;
        } catch (Exception e) {
            log.error("ignore", e);
            return null;
        }
    }

    public static long CalculateSize(@NotNull String rpath) {
        Path opath = Paths.get(rpath, "objects");
        long size = 0;
        try {
            /// only java8
            size = Files.walk(opath).filter(p -> p.toFile().isFile()).mapToLong(p -> p.toFile().length()).sum();
        } catch (Exception e) {
            log.error("error ", e);
        }
        return size;
    }

    /// Make uuid and stash
    public static String MakeUUID(@NotNull String ufile) {
        UUID self = UUID.randomUUID();
        String us = self.toString() + "\n";
        FileWriter fw;
        try {
            fw = new FileWriter(ufile);
            fw.write(us);
            fw.close();
        } catch (Exception e) {
            return null;
        }
        return us;
    }

    /// Repo uuid load
    public static String RepoUUID(@NotNull String baredir) {
        /// TBD when uuid not exists we new a uuid, if exists. we use it.
        try {
            File f = new File(baredir, "svn.uuid");
            if (!f.exists()) {
                return MakeUUID(baredir + "/svn.uuid");
            }
            if (f.isDirectory()) {
                log.error("invalid svn.uuid file type {}", baredir);
                return null;
            }
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String uline = br.readLine();
            br.close();
            return uline;
        } catch (Exception e) {
            return null;
        }
    }

    /// Get uuid
    public static String GetUUID(@NotNull String baredir) {
        try {
            File f = new File(baredir, "svn.uuid");
            if (!f.exists()) {
                return MakeUUID(baredir + "/svn.uuid");
            }
            if (f.isDirectory()) {
                log.error("path svn.uuid is directory {}", baredir);
                return null;
            }
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String uline = br.readLine();
            br.close();
            return uline;
        } catch (Exception e) {
            return null;
        }
    }

}