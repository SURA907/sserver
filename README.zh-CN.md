# Overview
Subversion frontend server for git repository

## Origin

This Project fork from git-as-svn in github/bozaro/git-as-svn
[Origin ReadMe](./README.md)
[Github Repository](https://github.com/bozaro/git-as-svn)
Origin Repository Author: Artem V. Navrotskiy.

## Usage

Please download origin repo binary for other user

## Report Bug

如果你需要反馈问题，可以点击： [create issues](https://gitee.com/oscstudio/sserver/issues/new)

如果你需要报告安全漏洞请发送邮件到 git@oschina.cn


